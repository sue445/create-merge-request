package cmr_test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/sue445/create-merge-request"
	"testing"
)

func TestToBranchSuffix_Found(t *testing.T) {
	tests := []struct {
		str  string
		want cmr.BranchSuffix
	}{
		{
			str:  "none",
			want: cmr.None,
		},
		{
			str:  "random",
			want: cmr.Random,
		},
		{
			str:  "timestamp",
			want: cmr.Timestamp,
		},
		{
			str:  "short-commit-hash",
			want: cmr.ShortCommitHash,
		},
	}
	for _, tt := range tests {
		t.Run(tt.str, func(t *testing.T) {
			got, err := cmr.ToBranchSuffix(tt.str)

			if assert.NoError(t, err) {
				assert.Equal(t, tt.want, got)
			}
		})
	}
}

func TestToBranchSuffix_NotFound(t *testing.T) {
	_, err := cmr.ToBranchSuffix("foo")

	assert.EqualError(t, err, "foo is unknown BranchSuffix type")
}
