package cmr_test

import (
	"github.com/go-git/go-git/v5"
	"github.com/jarcoal/httpmock"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/api/client-go"
	"gitlab.com/sue445/create-merge-request"
	"net/http"
	"testing"
)

func TestGitLab_GetUserID(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	httpmock.RegisterResponder("GET", "https://gitlab.example.com/api/v4/users?username=john_smith",
		httpmock.NewStringResponder(200, readTestFile(t, "get_user_1.json")))

	g, err := cmr.NewGitLab(&cmr.GitLabParams{
		APIEndpoint:  "https://gitlab.example.com/api/v4",
		PrivateToken: "XXXXXX",
		HTTPClient:   http.DefaultClient,
		LogLevel:     "DEBUG",
	})

	if assert.NoError(t, err) {
		userID, err := g.GetUserID("john_smith")
		if assert.NoError(t, err) {
			assert.Equal(t, 1, userID)
		}
	}
}

func TestGitLab_GetUserIDs(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	httpmock.RegisterResponder("GET", "https://gitlab.example.com/api/v4/users?username=john_smith",
		httpmock.NewStringResponder(200, readTestFile(t, "get_user_1.json")))

	httpmock.RegisterResponder("GET", "https://gitlab.example.com/api/v4/users?username=jack_smith",
		httpmock.NewStringResponder(200, readTestFile(t, "get_user_2.json")))

	g, err := cmr.NewGitLab(&cmr.GitLabParams{
		APIEndpoint:  "https://gitlab.example.com/api/v4",
		PrivateToken: "XXXXXX",
		HTTPClient:   http.DefaultClient,
		LogLevel:     "DEBUG",
	})

	if assert.NoError(t, err) {
		userIDs, err := g.GetUserIDs([]string{"john_smith", "jack_smith"})
		if assert.NoError(t, err) {
			assert.Len(t, userIDs, 2)
			assert.Contains(t, userIDs, 1)
			assert.Contains(t, userIDs, 2)
		}
	}
}

func TestGitLab_CommitFiles(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	httpmock.RegisterResponder("POST", "https://gitlab.example.com/api/v4/projects/my-group%2Fmy-project/repository/commits",
		httpmock.NewStringResponder(200, `{}`))

	g, err := cmr.NewGitLab(&cmr.GitLabParams{
		APIEndpoint:  "https://gitlab.example.com/api/v4",
		PrivateToken: "XXXXXX",
		HTTPClient:   http.DefaultClient,
		LogLevel:     "DEBUG",
	})

	if assert.NoError(t, err) {
		err = g.CommitFiles(&cmr.CommitFilesParams{
			ProjectName:   "my-group/my-project",
			NewBranch:     "patch",
			StartBranch:   "main",
			AuthorEmail:   "test@example.com",
			AuthorName:    "test",
			CommitMessage: "commit files",
			Files: map[string]*git.FileStatus{
				testdataPath("will_be_changed.txt"): {Staging: git.Unmodified, Worktree: git.Modified},
				testdataPath("will_be_removed.txt"): {Staging: git.Unmodified, Worktree: git.Deleted},
				testdataPath("new.txt"):             {Staging: git.Untracked, Worktree: git.Untracked},
			},
		})
		assert.NoError(t, err)
	}
}

func TestGitLab_CreateMergeRequest(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	httpmock.RegisterResponder("POST", "https://gitlab.example.com/api/v4/projects/my-group%2Fmy-project/merge_requests",
		httpmock.NewStringResponder(200, readTestFile(t, "create_merge_request.json")))

	httpmock.RegisterResponder("GET", "https://gitlab.example.com/api/v4/users?username=john_smith",
		httpmock.NewStringResponder(200, readTestFile(t, "get_user_1.json")))

	httpmock.RegisterResponder("GET", "https://gitlab.example.com/api/v4/users?username=jack_smith",
		httpmock.NewStringResponder(200, readTestFile(t, "get_user_2.json")))

	g, err := cmr.NewGitLab(&cmr.GitLabParams{
		APIEndpoint:  "https://gitlab.example.com/api/v4",
		PrivateToken: "XXXXXX",
		HTTPClient:   http.DefaultClient,
		LogLevel:     "DEBUG",
	})

	if assert.NoError(t, err) {
		mr, err := g.CreateMergeRequest(&cmr.CreateMergeRequestParams{
			ProjectName:  "my-group/my-project",
			SourceBranch: "patch",
			TargetBranch: "main",
			Title:        "title",
			Description:  "description",
			Labels:       []string{"create-merge-request"},
			Assignees:    []string{"john_smith", "jack_smith"},
			Reviewers:    []string{"john_smith", "jack_smith"},
		})
		if assert.NoError(t, err) {
			assert.Equal(t, 1, mr.ID)
		}
	}
}

func TestGitLab_IsExistsBranch(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	httpmock.RegisterResponder("GET", "https://gitlab.example.com/api/v4/projects/my-group%2Fmy-project/repository/branches/main",
		httpmock.NewStringResponder(200, readTestFile(t, "get_branch_exists.json")))

	httpmock.RegisterResponder("GET", "https://gitlab.example.com/api/v4/projects/my-group%2Fmy-project/repository/branches/unknown",
		httpmock.NewStringResponder(404, readTestFile(t, "get_branch_not_exists.json")))

	g, err := cmr.NewGitLab(&cmr.GitLabParams{
		APIEndpoint:  "https://gitlab.example.com/api/v4",
		PrivateToken: "XXXXXX",
		HTTPClient:   http.DefaultClient,
		LogLevel:     "DEBUG",
	})

	require.NoError(t, err)

	tests := []struct {
		name   string
		params *cmr.IsExistsBranchParams
		want   bool
	}{
		{
			name: "exists",
			params: &cmr.IsExistsBranchParams{
				ProjectName: "my-group/my-project",
				Branch:      "main",
			},
			want: true,
		},
		{
			name: "not exists",
			params: &cmr.IsExistsBranchParams{
				ProjectName: "my-group/my-project",
				Branch:      "unknown",
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := g.IsExistsBranch(tt.params)
			if assert.NoError(t, err) {
				assert.Equal(t, tt.want, got)
			}
		})
	}
}

func TestToCommitActionOptions(t *testing.T) {
	type args struct {
		filepath string
		status   *git.FileStatus
	}
	tests := []struct {
		args args
		want *gitlab.CommitActionOptions
	}{
		{
			args: args{
				filepath: testdataPath("will_be_changed.txt"),
				status:   &git.FileStatus{Staging: git.Unmodified, Worktree: git.Modified},
			},
			want: &gitlab.CommitActionOptions{
				Action:          gitlab.Ptr(gitlab.FileUpdate),
				FilePath:        gitlab.Ptr(testdataPath("will_be_changed.txt")),
				Content:         gitlab.Ptr("YmVmb3JlCg=="), // "before\n"
				Encoding:        gitlab.Ptr("base64"),
				ExecuteFilemode: gitlab.Ptr(false),
			},
		},
		{
			args: args{
				filepath: testdataPath("will_be_removed.txt"),
				status:   &git.FileStatus{Staging: git.Unmodified, Worktree: git.Deleted},
			},
			want: &gitlab.CommitActionOptions{
				Action:   gitlab.Ptr(gitlab.FileDelete),
				FilePath: gitlab.Ptr(testdataPath("will_be_removed.txt")),
			},
		},
		{
			args: args{
				filepath: testdataPath("new.txt"),
				status:   &git.FileStatus{Staging: git.Untracked, Worktree: git.Untracked},
			},
			want: &gitlab.CommitActionOptions{
				Action:          gitlab.Ptr(gitlab.FileCreate),
				FilePath:        gitlab.Ptr(testdataPath("new.txt")),
				Content:         gitlab.Ptr(""),
				Encoding:        gitlab.Ptr("base64"),
				ExecuteFilemode: gitlab.Ptr(false),
			},
		},
		{
			args: args{
				filepath: testdataPath("test.sh"),
				status:   &git.FileStatus{Staging: git.Untracked, Worktree: git.Untracked},
			},
			want: &gitlab.CommitActionOptions{
				Action:          gitlab.Ptr(gitlab.FileCreate),
				FilePath:        gitlab.Ptr(testdataPath("test.sh")),
				Content:         gitlab.Ptr("IyEvYmluL2Jhc2gKCmVjaG8gdGVzdAo="), // "#!/bin/bash\n\necho test\n"
				Encoding:        gitlab.Ptr("base64"),
				ExecuteFilemode: gitlab.Ptr(true),
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.args.filepath, func(t *testing.T) {
			got, err := cmr.ToCommitActionOptions(tt.args.filepath, tt.args.status)
			if assert.NoError(t, err) {
				assert.Equal(t, tt.want, got)
			}
		})
	}
}
