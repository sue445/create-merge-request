# Requirements: git, go
NAME     := create-merge-request
VERSION  := $(shell cat VERSION)
REVISION := $(shell git rev-parse --short HEAD)

SRCS    := $(shell find . -type f -name '*.go')
LDFLAGS := "-s -w -X \"main.Version=$(VERSION)\" -X \"main.Revision=$(REVISION)\""

.DEFAULT_GOAL := bin/$(NAME)

branch_suffix_string.go: branch_suffix.go
	go generate branch_suffix.go

.PHONY: generate
generate: branch_suffix_string.go

.PHONY: generate-check-diff
generate-check-diff: generate
	@if ! git diff --quiet; then  \
		echo "Error: There are uncommitted changes in generated files."; \
		exit 1; \
	fi

bin/$(NAME): $(SRCS) generate
	go build -ldflags=$(LDFLAGS) -o bin/$(NAME) ./cmd/$(NAME)/main.go

.PHONY: clean
clean:
	rm -rf bin/*

.PHONY: test
test: generate
	go test -count=1 $${TEST_ARGS} ./...

.PHONY: testrace
testrace: generate
	go test -count=1 $${TEST_ARGS} -race ./...

.PHONY: fmt
fmt:
	go fmt ./...

.PHONY: tag
tag:
	git tag -a $(VERSION) -m "Release $(VERSION)"
	git push --tags

.PHONY: release
release: tag
	git push origin main
