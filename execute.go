package cmr

import (
	"fmt"
	"github.com/cockroachdb/errors"
	"os"
)

// ExecuteParams represents parameters of Execute
type ExecuteParams struct {
	GitLabParams GitLabParams
	ProjectName  string
	SourceBranch SourceBranch

	// commit params
	AuthorEmail   string
	AuthorName    string
	CommitMessage string

	// MR params
	TargetBranch string
	Title        string
	Description  string
	Labels       []string
	Assignees    []string
	Reviewers    []string
	AutoMerge    bool
}

// Execute runs create-merge-request
func Execute(params *ExecuteParams) error {
	currentDir, err := os.Getwd()
	if err != nil {
		return errors.WithStack(err)
	}

	files, err := GetUntrackedFiles(currentDir)
	if err != nil {
		return errors.WithStack(err)
	}

	if len(files) < 1 {
		fmt.Println("[INFO] create-merge-request is skipped. (nothing to commit)")
		return nil
	}

	g, err := NewGitLab(&params.GitLabParams)
	if err != nil {
		return errors.WithStack(err)
	}

	sourceBranch, err := params.SourceBranch.GenerateBranchName(currentDir)
	if err != nil {
		return errors.WithStack(err)
	}

	exists, err := g.IsExistsBranch(&IsExistsBranchParams{
		ProjectName: params.ProjectName,
		Branch:      sourceBranch,
	})
	if err != nil {
		return errors.WithStack(err)
	}

	if exists {
		fmt.Printf("[INFO] create-merge-request is skipped. (%s branch is already exists in %s project)\n", sourceBranch, params.ProjectName)
		return nil
	}

	err = g.CommitFiles(&CommitFilesParams{
		ProjectName:   params.ProjectName,
		NewBranch:     sourceBranch,
		StartBranch:   params.TargetBranch,
		AuthorName:    params.AuthorName,
		AuthorEmail:   params.AuthorEmail,
		CommitMessage: params.CommitMessage,
		Files:         files,
	})
	if err != nil {
		return errors.WithStack(err)
	}

	mr, err := g.CreateMergeRequest(&CreateMergeRequestParams{
		ProjectName:  params.ProjectName,
		SourceBranch: sourceBranch,
		TargetBranch: params.TargetBranch,
		Title:        params.Title,
		Description:  params.Description,
		Labels:       params.Labels,
		Assignees:    params.Assignees,
		Reviewers:    params.Reviewers,
	})
	if err != nil {
		return errors.WithStack(err)
	}

	fmt.Printf("[INFO] Created Merge Request: %s\n", mr.WebURL)

	if !params.AutoMerge {
		return nil
	}

	err = g.AcceptMergeRequest(&AcceptMergeRequestParams{
		ProjectName:     params.ProjectName,
		MergeRequestIID: mr.IID,
	})
	if err != nil {
		return errors.WithStack(err)
	}

	return nil
}
