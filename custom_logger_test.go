package cmr_test

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	cmr "gitlab.com/sue445/create-merge-request"
	"testing"
)

func TestConvertToMap(t *testing.T) {
	tests := []struct {
		keysAndValues []interface{}
		want          map[string]interface{}
	}{
		{
			keysAndValues: []interface{}{"key1", "value1", "key2", 2},
			want:          map[string]interface{}{"key1": "value1", "key2": 2},
		},
	}
	for _, tt := range tests {
		t.Run(fmt.Sprintf("%v", tt.keysAndValues), func(t *testing.T) {
			got := cmr.ConvertToMap(tt.keysAndValues)
			assert.Equal(t, tt.want, got)
		})
	}
}
