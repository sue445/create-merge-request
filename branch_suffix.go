//go:generate go run golang.org/x/tools/cmd/stringer -type BranchSuffix -output branch_suffix_string.go
package cmr

import "github.com/cockroachdb/errors"

// BranchSuffix represents branch suffix format
type BranchSuffix int

const (
	// None doesn't append suffix to branches
	None BranchSuffix = iota

	// Random append a random string to the branch name specified in `--source-branch`. (e.g. `create-merge-request/patch-xxxxxxx`)
	Random

	// Timestamp append an Unix Epoch Time to the branch name specified in `--source-branch`. (e.g. `create-merge-request/patch-1719675225`)
	Timestamp

	// ShortCommitHash append a git short hash to the branch name specified in `--source-branch`. (e.g. `create-merge-request/patch-123abcd`)
	ShortCommitHash
)

// ToBranchSuffix returns BranchSuffix from string
func ToBranchSuffix(str string) (BranchSuffix, error) {
	switch str {
	case "none":
		return None, nil
	case "random":
		return Random, nil
	case "timestamp":
		return Timestamp, nil
	case "short-commit-hash":
		return ShortCommitHash, nil
	}

	return None, errors.Newf("%s is unknown BranchSuffix type", str)
}
