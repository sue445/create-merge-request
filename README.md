# create-merge-request
Create a Merge Request for changes to your [GitLab™](https://about.gitlab.com/) project

_GITLAB is a trademark of GitLab Inc. in the United States and other countries and regions_

This is inspired by https://github.com/peter-evans/create-pull-request

<details>
  <summary>TOC</summary>

[[_TOC_]]

</details>

## Requirements
* GitLab API v4
* GitLab Access Token
  * [Personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) (for 2+ projects) or [Project access token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html) (for 1 project)
  * Role: Developer
  * Scopes: `api`

_create-merge-request_ uses https://github.com/go-git/go-git for git operations.
So this doesn't rely on the system `git` command.

## Simple usage for GitLab CI
First of all, set the GitLab Access Token to [CI/CD Variables](https://docs.gitlab.com/ee/ci/variables/) named `GITLAB_ACCESS_TOKEN`

Example `.gitlab-ci.yml`

```yaml
stages:
  - build

create_mr_for_changes:
  stage: build

  image: debian:stable-slim

  before_script:
    # Download and install latest create-merge-request
    - apt-get update
    - apt-get install -y curl
    - VERSION=$(curl -s --fail https://gitlab.com/sue445/create-merge-request/-/raw/main/VERSION?ref_type=heads)
    - pushd /tmp
    - curl --retry 3 -L -o create-merge-request.tar.gz https://gitlab.com/sue445/create-merge-request/-/releases/${VERSION}/downloads/create-merge-request_Linux_x86_64.tar.gz
    - tar xzvf create-merge-request.tar.gz
    - mv create-merge-request /usr/local/bin
    - popd

  script:
    # TODO: Add your changes
    - date > now.txt

    # Create Merge Request if repo is changed
    - create-merge-request

  rules:
    # Run only scheduled pipeline
    # ref. https://docs.gitlab.com/ee/ci/pipelines/schedules.html
    - if: $CI_PIPELINE_SOURCE == "schedule"
```

Here is the Merge Request actually created :point_right: https://gitlab.com/sue445/create-merge-request-sandbox/-/merge_requests/2

## Install
Download binary from https://gitlab.com/sue445/create-merge-request/-/releases

## Command
```bash
$ create-merge-request --help
NAME:
   create-merge-request - Create a Merge Request for changes to your GitLab project

USAGE:
   create-merge-request [command options]

REQUIRED PARAMETERS:
   --gitlab-api-endpoint value  GitLab API Endpoint (e.g. https://gitlab.com/api/v4) [$GITLAB_API_ENDPOINT, $CI_API_V4_URL]
   --gitlab-access-token value  GitLab access token [$GITLAB_ACCESS_TOKEN]
   --gitlab-project value       GitLab Project Path (e.g. gitlab-org/gitlab) [$GITLAB_PROJECT, $CI_PROJECT_PATH]

OPTIONAL PARAMETERS:
   --author-email value                     author email for commit (default: ${GITLAB_USER_LOGIN}@noreply.${CI_SERVER_HOST} (e.g. create-merge-request@noreply.example.com))
   --author-name value                      author name for commit (default: create-merge-request) [$GITLAB_USER_NAME]
   --commit-message value, -m value         commit message (default: [create-merge-request] automated change)
   --source-branch value                    Merge Request branch name (default: create-merge-request/patch)
   --source-branch-suffix value             Merge Request branch name suffix (none,random,timestamp,short-commit-hash) (default: none)
   --target-branch value                    Send Merge Request to this branch (e.g. main, master) (default: main) [$CI_DEFAULT_BRANCH]
   --title value, -t value                  Merge Request title (default: Changes by create-merge-request)
   --description value                      Merge Request description (default: Automated changes by [create-merge-request](https://gitlab.com/sue445/create-merge-request))
   --labels value [ --labels value ]        Merge Request labels
   --assignees value [ --assignees value ]  Merge Request assignees (e.g. user1,user2)
   --reviewers value [ --reviewers value ]  Merge Request reviewers (e.g. user1,user2)
   --auto-merge                             Merge the Merge Request when the pipeline succeeds (default: disabled)
   --log-level value                        Log level (e.g. DEBUG, INFO, WARN, ERROR) (default: INFO)
   --help, -h                               show help
   --version, -v                            print the version
```

### `--source-branch-suffix`
By default(`--source-branch-suffix=none`), if a branch already exists in repository, a new branch with the same name will not be created.

To change this behavior, set the following in the `--source-branch-suffix`.

* `random` : Append a random string to the branch name specified in `--source-branch`. (e.g. `create-merge-request/patch-xxxxxxx`)
* `timestamp` : Append an Unix Epoch Time to the branch name specified in `--source-branch`. (e.g. `create-merge-request/patch-1719675225`)
* `short-commit-hash` : Append a git short hash to the branch name specified in `--source-branch`. (e.g. `create-merge-request/patch-123abcd`)

### `--auto-merge`
You may want to merge immediately Merge Request when pipeline succeeds.

In such a case, please use `--auto-merge`

See more information :point_right: https://docs.gitlab.com/ee/user/project/merge_requests/auto_merge.html

#### Trouble Shooting: 401 Unauthorized
The reason is that target branch (`--target-branch`, often `main` or `master`) that the Merge Request is merging into is protected.

https://docs.gitlab.com/ee/user/project/protected_branches.html

Please do one of the following.

1. Change "Allowed to merge" from "Maintainers" to **"Developers + Maintainers"**
2. Change Access Token role from Developer to **Maintainer**

#### Trouble Shooting: 405 Method Not Allowed
If "Merge a merge request API" is called immediately after the Merge Request is created, 405 error will be returned.

_create-merge-request_ will automatically retry 405 errors, but if it still fails, the build may not have been triggered for the Merge Request.
