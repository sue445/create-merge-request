package cmr_test

import (
	"github.com/cockroachdb/errors"
	"gitlab.com/sue445/create-merge-request"
	"io"
	"os"
	"os/exec"
	"path/filepath"
	"testing"
)

func setupTestRepo(gitRepo string, changeRepo bool) error {
	files := []string{
		"unchanged.txt",
		"will_be_changed.txt",
		"will_be_removed.txt",
		"chmod_644_to_755.sh",
		"chmod_755_to_644.sh",
	}

	for _, file := range files {
		err := copyFileToDir(testdataPath(file), gitRepo)
		if err != nil {
			return errors.WithStack(err)
		}
	}

	err := chdir(gitRepo, func() error {
		// git init
		return exec.Command("git", "init").Run()
	})

	if err != nil {
		return errors.WithStack(err)
	}

	err = copyFile(testdataPath("_gitconfig"), filepath.Join(gitRepo, ".git", "config"))
	if err != nil {
		return errors.WithStack(err)
	}

	err = chdir(gitRepo, func() error {
		// git add .
		err = exec.Command("git", "add", ".").Run()
		if err != nil {
			return errors.WithStack(err)
		}

		// git commit -am 'initial commit'
		err = exec.Command("git", "commit", "-am", "'initial commit'").Run()
		if err != nil {
			return errors.WithStack(err)
		}
		return nil
	})

	if err != nil {
		return errors.WithStack(err)
	}

	if !changeRepo {
		return nil
	}

	// change repo
	err = copyFileToDir(testdataPath("new.txt"), gitRepo)
	if err != nil {
		return errors.WithStack(err)
	}

	err = copyFile(testdataPath("will_be_changed_after.txt"), filepath.Join(gitRepo, "will_be_changed.txt"))
	if err != nil {
		return errors.WithStack(err)
	}

	err = os.Remove(filepath.Join(gitRepo, "will_be_removed.txt"))
	if err != nil {
		return errors.WithStack(err)
	}

	err = os.Chmod(filepath.Join(gitRepo, "chmod_644_to_755.sh"), 0755)
	if err != nil {
		return errors.WithStack(err)
	}

	err = os.Chmod(filepath.Join(gitRepo, "chmod_755_to_644.sh"), 0644)
	if err != nil {
		return errors.WithStack(err)
	}

	return nil
}

func copyFileToDir(srcFile string, destDir string) error {
	destFile := filepath.Join(destDir, filepath.Base(srcFile))
	return copyFile(srcFile, destFile)
}

func copyFile(srcFile string, destFile string) error {
	in, err := os.Open(srcFile)
	if err != nil {
		return errors.WithStack(err)
	}
	defer in.Close()

	out, err := os.Create(destFile)
	if err != nil {
		return errors.WithStack(err)
	}
	defer out.Close()

	_, err = io.Copy(out, in)
	if err != nil {
		return errors.WithStack(err)
	}

	inInfo, err := in.Stat()
	if err != nil {
		return errors.WithStack(err)
	}

	err = os.Chmod(destFile, inInfo.Mode())
	if err != nil {
		return errors.WithStack(err)
	}

	return nil
}

func chdir(dir string, fn func() error) error {
	originalDir, err := os.Getwd()
	if err != nil {
		return errors.WithStack(err)
	}

	err = os.Chdir(dir)
	if err != nil {
		return errors.WithStack(err)
	}

	defer func() {
		err := os.Chdir(originalDir)
		if err != nil {
			panic(err)
		}
	}()

	err = fn()

	if err != nil {
		return errors.WithStack(err)
	}

	return nil
}

func testdataPath(path string) string {
	return filepath.Join("testdata", path)
}

func readTestFile(t *testing.T, file string) string {
	data, err := cmr.ReadTextFile(testdataPath(file))

	if err != nil {
		t.Fatalf("Can't read file: %s, %+v", file, err)
	}

	return data
}
