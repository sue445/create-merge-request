package cmr_test

import (
	"github.com/jarcoal/httpmock"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/sue445/create-merge-request"
	"net/http"
	"testing"
)

func TestExecute_Updated(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	httpmock.RegisterResponder("POST", "https://gitlab.example.com/api/v4/projects/my-group%2Fmy-project/repository/commits",
		httpmock.NewStringResponder(200, `{}`))

	httpmock.RegisterResponder("POST", "https://gitlab.example.com/api/v4/projects/my-group%2Fmy-project/merge_requests",
		httpmock.NewStringResponder(200, readTestFile(t, "create_merge_request.json")))

	httpmock.RegisterResponder("GET", "https://gitlab.example.com/api/v4/users?username=john_smith",
		httpmock.NewStringResponder(200, readTestFile(t, "get_user_1.json")))

	httpmock.RegisterResponder("GET", "https://gitlab.example.com/api/v4/users?username=jack_smith",
		httpmock.NewStringResponder(200, readTestFile(t, "get_user_2.json")))

	httpmock.RegisterResponder("GET", "https://gitlab.example.com/api/v4/projects/my-group%2Fmy-project/repository/branches/patch",
		httpmock.NewStringResponder(404, readTestFile(t, "get_branch_not_exists.json")))

	gitRepo := t.TempDir()

	err := setupTestRepo(gitRepo, true)
	if err != nil {
		t.Fatalf("setupTestRepo is failed: %+v", err)
	}

	params := &cmr.ExecuteParams{
		ProjectName: "my-group/my-project",
		GitLabParams: cmr.GitLabParams{
			APIEndpoint:  "https://gitlab.example.com/api/v4",
			PrivateToken: "XXXXXX",
			HTTPClient:   http.DefaultClient,
			LogLevel:     "DEBUG",
		},

		AuthorEmail: "test@example.com",
		AuthorName:  "test",
		SourceBranch: cmr.SourceBranch{
			Name:   "patch",
			Suffix: cmr.None,
		},
		TargetBranch: "main",
		Title:        "title",
		Description:  "description",
		Labels:       []string{"create-merge-request"},
		Assignees:    []string{"john_smith", "jack_smith"},
		Reviewers:    []string{"john_smith", "jack_smith"},
		AutoMerge:    false,
	}

	err = chdir(gitRepo, func() error {
		return cmr.Execute(params)
	})
	assert.NoError(t, err)
}

func TestExecute_NotUpdated(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	gitRepo := t.TempDir()

	err := setupTestRepo(gitRepo, false)
	if err != nil {
		t.Fatalf("setupTestRepo is failed: %+v", err)
	}

	params := &cmr.ExecuteParams{
		ProjectName: "my-group/my-project",
		GitLabParams: cmr.GitLabParams{
			APIEndpoint:  "https://gitlab.example.com/api/v4",
			PrivateToken: "XXXXXX",
			HTTPClient:   http.DefaultClient,
		},

		AuthorEmail: "test@example.com",
		AuthorName:  "test",
		SourceBranch: cmr.SourceBranch{
			Name:   "patch",
			Suffix: cmr.None,
		},
		TargetBranch: "main",
		Title:        "title",
		Description:  "description",
		Labels:       []string{"create-merge-request"},
		Assignees:    []string{"john_smith", "jack_smith"},
		Reviewers:    []string{"john_smith", "jack_smith"},
		AutoMerge:    false,
	}

	err = chdir(gitRepo, func() error {
		return cmr.Execute(params)
	})
	assert.NoError(t, err)
}

func TestExecute_DuplicateBranch(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	httpmock.RegisterResponder("GET", "https://gitlab.example.com/api/v4/projects/my-group%2Fmy-project/repository/branches/patch",
		httpmock.NewStringResponder(200, readTestFile(t, "get_branch_exists.json")))

	gitRepo := t.TempDir()

	err := setupTestRepo(gitRepo, true)
	if err != nil {
		t.Fatalf("setupTestRepo is failed: %+v", err)
	}

	params := &cmr.ExecuteParams{
		ProjectName: "my-group/my-project",
		GitLabParams: cmr.GitLabParams{
			APIEndpoint:  "https://gitlab.example.com/api/v4",
			PrivateToken: "XXXXXX",
			HTTPClient:   http.DefaultClient,
			LogLevel:     "DEBUG",
		},

		AuthorEmail: "test@example.com",
		AuthorName:  "test",
		SourceBranch: cmr.SourceBranch{
			Name:   "patch",
			Suffix: cmr.None,
		},
		TargetBranch: "main",
		Title:        "title",
		Description:  "description",
		Labels:       []string{"create-merge-request"},
		Assignees:    []string{"john_smith", "jack_smith"},
		Reviewers:    []string{"john_smith", "jack_smith"},
		AutoMerge:    false,
	}

	err = chdir(gitRepo, func() error {
		return cmr.Execute(params)
	})
	assert.NoError(t, err)
}

func TestExecute_UpdatedAndAutoMerge(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	httpmock.RegisterResponder("POST", "https://gitlab.example.com/api/v4/projects/my-group%2Fmy-project/repository/commits",
		httpmock.NewStringResponder(200, `{}`))

	httpmock.RegisterResponder("POST", "https://gitlab.example.com/api/v4/projects/my-group%2Fmy-project/merge_requests",
		httpmock.NewStringResponder(200, readTestFile(t, "create_merge_request.json")))

	httpmock.RegisterResponder("GET", "https://gitlab.example.com/api/v4/users?username=john_smith",
		httpmock.NewStringResponder(200, readTestFile(t, "get_user_1.json")))

	httpmock.RegisterResponder("GET", "https://gitlab.example.com/api/v4/users?username=jack_smith",
		httpmock.NewStringResponder(200, readTestFile(t, "get_user_2.json")))

	httpmock.RegisterResponder("GET", "https://gitlab.example.com/api/v4/projects/my-group%2Fmy-project/repository/branches/patch",
		httpmock.NewStringResponder(404, readTestFile(t, "get_branch_not_exists.json")))

	httpmock.RegisterResponder("PUT", "https://gitlab.example.com/api/v4/projects/my-group%2Fmy-project/merge_requests/1/merge",
		httpmock.NewStringResponder(200, `{}`))

	gitRepo := t.TempDir()

	err := setupTestRepo(gitRepo, true)
	if err != nil {
		t.Fatalf("setupTestRepo is failed: %+v", err)
	}

	params := &cmr.ExecuteParams{
		ProjectName: "my-group/my-project",
		GitLabParams: cmr.GitLabParams{
			APIEndpoint:  "https://gitlab.example.com/api/v4",
			PrivateToken: "XXXXXX",
			HTTPClient:   http.DefaultClient,
			LogLevel:     "DEBUG",
		},

		AuthorEmail: "test@example.com",
		AuthorName:  "test",
		SourceBranch: cmr.SourceBranch{
			Name:   "patch",
			Suffix: cmr.None,
		},
		TargetBranch: "main",
		Title:        "title",
		Description:  "description",
		Labels:       []string{"create-merge-request"},
		Assignees:    []string{"john_smith", "jack_smith"},
		Reviewers:    []string{"john_smith", "jack_smith"},
		AutoMerge:    true,
	}

	err = chdir(gitRepo, func() error {
		return cmr.Execute(params)
	})
	require.NoError(t, err)

	httpmock.GetTotalCallCount()
	info := httpmock.GetCallCountInfo()

	calledAutoMergeCount := info["PUT https://gitlab.example.com/api/v4/projects/my-group%2Fmy-project/merge_requests/1/merge"]
	assert.Equal(t, 1, calledAutoMergeCount)
}
