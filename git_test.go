package cmr_test

import (
	"github.com/go-git/go-git/v5"
	"github.com/stretchr/testify/assert"
	"gitlab.com/sue445/create-merge-request"
	"golang.org/x/exp/maps"
	"golang.org/x/exp/slices"
	"testing"
)

func TestGetUntrackedFiles(t *testing.T) {
	gitRepo := t.TempDir()

	err := setupTestRepo(gitRepo, true)
	if err != nil {
		t.Fatalf("setupTestRepo is failed: %+v", err)
	}

	got, err := cmr.GetUntrackedFiles(gitRepo)
	if assert.NoError(t, err) {
		wantFileNames := []string{
			"chmod_644_to_755.sh",
			"chmod_755_to_644.sh",
			"new.txt",
			"will_be_changed.txt",
			"will_be_removed.txt",
		}
		gotFileNames := maps.Keys(got)
		slices.Sort(gotFileNames)
		assert.Equal(t, wantFileNames, gotFileNames)

		assert.Equal(t, &git.FileStatus{Staging: git.Unmodified, Worktree: git.Modified}, got["will_be_changed.txt"], "will_be_changed.txt is invalid")
		assert.Equal(t, &git.FileStatus{Staging: git.Unmodified, Worktree: git.Deleted}, got["will_be_removed.txt"], "will_be_removed.txt is invalid")
		assert.Equal(t, &git.FileStatus{Staging: git.Untracked, Worktree: git.Untracked}, got["new.txt"], "new.txt is invalid")
		assert.Equal(t, &git.FileStatus{Staging: git.Unmodified, Worktree: git.Modified}, got["chmod_644_to_755.sh"], "chmod_644_to_755.sh is invalid")
		assert.Equal(t, &git.FileStatus{Staging: git.Unmodified, Worktree: git.Modified}, got["chmod_755_to_644.sh"], "chmod_755_to_644.sh is invalid")
	}
}

func TestGetCurrentShortHash(t *testing.T) {
	gitRepo := t.TempDir()

	err := setupTestRepo(gitRepo, false)
	if err != nil {
		t.Fatalf("setupTestRepo is failed: %+v", err)
	}

	hash, err := cmr.GetCurrentShortHash(gitRepo)
	if assert.NoError(t, err) {
		assert.Regexp(t, "^[0-9a-f]{7}$", hash)
	}
}
