package cmr_test

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/sue445/create-merge-request"
	"testing"
)

func TestSourceBranch_GenerateBranchName(t *testing.T) {
	gitRepo := t.TempDir()

	err := setupTestRepo(gitRepo, true)
	if err != nil {
		t.Fatalf("setupTestRepo is failed: %+v", err)
	}

	type fields struct {
		Name   string
		Suffix cmr.BranchSuffix
	}
	tests := []struct {
		fields     fields
		wantRegexp string
	}{
		{
			fields: fields{
				Name:   "patch",
				Suffix: cmr.None,
			},
			wantRegexp: "^patch$",
		},
		{
			fields: fields{
				Name:   "patch",
				Suffix: cmr.Random,
			},
			wantRegexp: "^patch-[0-9A-Za-z]+$",
		},
		{
			fields: fields{
				Name:   "patch",
				Suffix: cmr.Timestamp,
			},
			wantRegexp: "^patch-[0-9]+$",
		},
		{
			fields: fields{
				Name:   "patch",
				Suffix: cmr.ShortCommitHash,
			},
			wantRegexp: "^patch-[0-9a-f]+$",
		},
	}
	for _, tt := range tests {
		t.Run(fmt.Sprintf("%s+%s", tt.fields.Name, tt.fields.Suffix.String()), func(t *testing.T) {
			s := &cmr.SourceBranch{
				Name:   tt.fields.Name,
				Suffix: tt.fields.Suffix,
			}
			got, err := s.GenerateBranchName(gitRepo)
			if assert.NoError(t, err) {
				assert.Regexp(t, tt.wantRegexp, got)
			}
		})
	}
}

func TestGenerateRandomString(t *testing.T) {
	got := cmr.GenerateRandomString()
	assert.Regexp(t, "^[0-9A-Za-z]{7}$", got)
}
