package cmr_test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/sue445/create-merge-request"
	"testing"
)

func TestIsExecutableFile(t *testing.T) {
	tests := []struct {
		name string
		file string
		want bool
	}{
		{
			name: "executable",
			file: testdataPath("test.sh"),
			want: true,
		},
		{
			name: "not executable",
			file: testdataPath("new.txt"),
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := cmr.IsExecutableFile(tt.file)
			if assert.NoError(t, err) {
				assert.Equal(t, tt.want, got)
			}
		})
	}
}
