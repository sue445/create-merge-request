package cmr

import (
	"github.com/cockroachdb/errors"
	"github.com/sirupsen/logrus"
	"os"
)

// CustomLogger adapts logrus to the LeveledLogger interface
type CustomLogger struct {
	logger *logrus.Logger
}

// NewCustomLogger returns new CustomLogger instance
//
// logLevel : DEBUG, INFO, WARN, ERROR
func NewCustomLogger(logLevel string) (*CustomLogger, error) {
	level, err := logrus.ParseLevel(logLevel)
	if err != nil {
		return nil, errors.Newf("Invalid log level: %v", err)
	}

	logrusLogger := logrus.New()
	logrusLogger.SetOutput(os.Stdout)
	logrusLogger.SetLevel(level)

	return &CustomLogger{logger: logrusLogger}, nil
}

// Error print error log
func (l *CustomLogger) Error(msg string, keysAndValues ...interface{}) {
	l.logger.WithFields(logrus.Fields(ConvertToMap(keysAndValues))).Error(msg)
}

// Info print info log
func (l *CustomLogger) Info(msg string, keysAndValues ...interface{}) {
	l.logger.WithFields(logrus.Fields(ConvertToMap(keysAndValues))).Info(msg)
}

// Debug print debug log
func (l *CustomLogger) Debug(msg string, keysAndValues ...interface{}) {
	l.logger.WithFields(logrus.Fields(ConvertToMap(keysAndValues))).Debug(msg)
}

// Warn print warn log
func (l *CustomLogger) Warn(msg string, keysAndValues ...interface{}) {
	l.logger.WithFields(logrus.Fields(ConvertToMap(keysAndValues))).Warn(msg)
}

// ConvertToMap convert from `[]interface{}` to `map[string]interface{}`
func ConvertToMap(keysAndValues []interface{}) map[string]interface{} {
	m := make(map[string]interface{})
	for i := 0; i < len(keysAndValues); i += 2 {
		if i+1 < len(keysAndValues) {
			m[keysAndValues[i].(string)] = keysAndValues[i+1]
		}
	}
	return m
}
