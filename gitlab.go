package cmr

import (
	"context"
	"github.com/cockroachdb/errors"
	"github.com/go-git/go-git/v5"
	"gitlab.com/gitlab-org/api/client-go"
	"golang.org/x/sync/errgroup"
	"net/http"
	"sync"
	"time"
)

// GitLab represents GitLab client
type GitLab struct {
	client         *gitlab.Client
	clientAcceptMR *gitlab.Client
	params         *GitLabParams
}

// GitLabParams represents parameters of NewGitLab
type GitLabParams struct {
	APIEndpoint  string
	PrivateToken string
	HTTPClient   *http.Client
	LogLevel     string
}

// NewGitLab returns new GitLab instance
func NewGitLab(params *GitLabParams) (*GitLab, error) {
	logger, err := NewCustomLogger(params.LogLevel)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	options := []gitlab.ClientOptionFunc{
		gitlab.WithBaseURL(params.APIEndpoint),
		gitlab.WithCustomLeveledLogger(logger),
	}
	if params.HTTPClient != nil {
		options = append(options, gitlab.WithHTTPClient(params.HTTPClient))
	}
	client, err := gitlab.NewClient(params.PrivateToken, options...)

	if err != nil {
		return nil, errors.WithStack(err)
	}

	optionsAcceptMR := []gitlab.ClientOptionFunc{
		gitlab.WithBaseURL(params.APIEndpoint),
		gitlab.WithCustomLeveledLogger(logger),
		gitlab.WithCustomRetry(retryHTTPCheckForAcceptMergeRequest),
		gitlab.WithCustomRetryWaitMinMax(500*time.Millisecond, 2000*time.Millisecond),
		gitlab.WithCustomRetryMax(10),
	}
	if params.HTTPClient != nil {
		optionsAcceptMR = append(optionsAcceptMR, gitlab.WithHTTPClient(params.HTTPClient))
	}
	clientAcceptMR, err := gitlab.NewClient(params.PrivateToken, optionsAcceptMR...)

	if err != nil {
		return nil, errors.WithStack(err)
	}

	return &GitLab{client: client, clientAcceptMR: clientAcceptMR, params: params}, nil
}

// retryHTTPCheckForAcceptMergeRequest for retry handler for AcceptMergeRequest
// This function patch to go-gitlab's retryHTTPCheck
// https://gitlab.com/gitlab-org/api/client-go/blob/v0.106.0/gitlab.go#L477
func retryHTTPCheckForAcceptMergeRequest(ctx context.Context, resp *http.Response, err error) (bool, error) {
	if ctx.Err() != nil {
		return false, errors.WithStack(ctx.Err())
	}
	if err != nil {
		return false, err
	}

	//if !c.disableRetries && (resp.StatusCode == 429 || resp.StatusCode >= 500) {
	//	return true, nil
	//}

	// If "Merge a merge request API" is called immediately after the Merge Request is created,
	// 405 or 422 error will be returned, so retry is necessary.
	//
	// c.f. https://docs.gitlab.com/ee/api/merge_requests.html#merge-a-merge-request
	if resp.StatusCode == 429 || resp.StatusCode >= 500 || resp.StatusCode == 405 || resp.StatusCode == 422 {
		return true, nil
	}
	return false, nil
}

// GetUserID returns userID from username
//
// ref. https://docs.gitlab.com/ee/api/users.html#list-users
func (g *GitLab) GetUserID(username string) (int, error) {
	users, _, err := g.client.Users.ListUsers(&gitlab.ListUsersOptions{Username: gitlab.Ptr(username)})
	if err != nil {
		return 0, errors.WithStack(err)
	}
	return users[0].ID, nil
}

// GetUserIDs returns userIDs from usernames
func (g *GitLab) GetUserIDs(usernames []string) ([]int, error) {
	var mutex = &sync.Mutex{}

	eg := errgroup.Group{}

	var userIDs []int
	for _, username := range usernames {
		// https://golang.org/doc/faq#closures_and_goroutines
		username := username

		eg.Go(func() error {
			userID, err := g.GetUserID(username)
			if err != nil {
				return errors.WithStack(err)
			}

			mutex.Lock()
			userIDs = append(userIDs, userID)
			mutex.Unlock()

			return nil
		})
	}

	err := eg.Wait()
	if err != nil {
		return []int{}, errors.WithStack(err)
	}
	return userIDs, nil
}

// CommitFilesParams represents parameters of CommitFiles
type CommitFilesParams struct {
	ProjectName   string
	NewBranch     string
	StartBranch   string
	AuthorEmail   string
	AuthorName    string
	CommitMessage string
	Files         git.Status
}

// CommitFiles commit files
//
// ref. https://docs.gitlab.com/ee/api/commits.html#create-a-commit-with-multiple-files-and-actions
func (g *GitLab) CommitFiles(params *CommitFilesParams) error {
	options := &gitlab.CreateCommitOptions{
		Branch:        gitlab.Ptr(params.NewBranch),
		StartBranch:   gitlab.Ptr(params.StartBranch),
		AuthorEmail:   gitlab.Ptr(params.AuthorEmail),
		AuthorName:    gitlab.Ptr(params.AuthorName),
		CommitMessage: gitlab.Ptr(params.CommitMessage),
		Actions:       []*gitlab.CommitActionOptions{},
	}

	for filepath, status := range params.Files {
		action, err := ToCommitActionOptions(filepath, status)
		if err != nil {
			return errors.WithStack(err)
		}
		options.Actions = append(options.Actions, action)
	}

	_, _, err := g.client.Commits.CreateCommit(
		params.ProjectName,
		options,
	)

	if err != nil {
		return errors.WithStack(err)
	}
	return nil
}

// CreateMergeRequestParams represents parameters of CreateMergeRequest
type CreateMergeRequestParams struct {
	ProjectName  string
	SourceBranch string
	TargetBranch string
	Title        string
	Description  string
	Labels       []string
	Assignees    []string
	Reviewers    []string
}

// CreateMergeRequest create MergeRequest
//
// ref. https://docs.gitlab.com/ee/api/merge_requests.html#create-mr
func (g *GitLab) CreateMergeRequest(params *CreateMergeRequestParams) (*gitlab.MergeRequest, error) {
	options := &gitlab.CreateMergeRequestOptions{
		SourceBranch:       gitlab.Ptr(params.SourceBranch),
		TargetBranch:       gitlab.Ptr(params.TargetBranch),
		Title:              gitlab.Ptr(params.Title),
		Description:        gitlab.Ptr(params.Description),
		RemoveSourceBranch: gitlab.Ptr(true),
	}

	if len(params.Labels) > 0 {
		labels := gitlab.LabelOptions{}
		for _, l := range params.Labels {
			labels = append(labels, l)
		}
		options.Labels = &labels
	}

	if len(params.Assignees) > 0 {
		userIDs, err := g.GetUserIDs(params.Assignees)
		if err != nil {
			return nil, errors.WithStack(err)
		}
		options.AssigneeIDs = &userIDs
	}

	if len(params.Reviewers) > 0 {
		userIDs, err := g.GetUserIDs(params.Reviewers)
		if err != nil {
			return nil, errors.WithStack(err)
		}
		options.ReviewerIDs = &userIDs
	}

	mr, _, err := g.client.MergeRequests.CreateMergeRequest(params.ProjectName, options)
	if err != nil {
		return nil, errors.WithStack(err)
	}
	return mr, nil
}

// IsExistsBranchParams represents parameters of IsExistsBranch
type IsExistsBranchParams struct {
	ProjectName string
	Branch      string
}

// IsExistsBranch check whether branch is existing
//
// ref. https://docs.gitlab.com/ee/api/branches.html#get-single-repository-branch
func (g *GitLab) IsExistsBranch(params *IsExistsBranchParams) (bool, error) {
	_, res, err := g.client.Branches.GetBranch(params.ProjectName, params.Branch)
	if err != nil {
		if res.StatusCode == http.StatusNotFound {
			return false, nil
		}
		return false, errors.WithStack(err)
	}

	return true, nil
}

// AcceptMergeRequestParams represents parameters of AcceptMergeRequest
type AcceptMergeRequestParams struct {
	ProjectName     string
	MergeRequestIID int
}

// AcceptMergeRequest set auto-merge to MergeRequest
//
// ref. https://docs.gitlab.com/ee/api/merge_requests.html#merge-a-merge-request
func (g *GitLab) AcceptMergeRequest(params *AcceptMergeRequestParams) error {
	_, _, err := g.clientAcceptMR.MergeRequests.AcceptMergeRequest(
		params.ProjectName, params.MergeRequestIID,
		&gitlab.AcceptMergeRequestOptions{MergeWhenPipelineSucceeds: gitlab.Ptr(true)},
	)

	if err != nil {
		return errors.WithStack(err)
	}

	return nil
}

// ToCommitActionOptions convert git.FileStatus to gitlab.CommitActionOptions
func ToCommitActionOptions(filepath string, status *git.FileStatus) (*gitlab.CommitActionOptions, error) {
	options := &gitlab.CommitActionOptions{FilePath: gitlab.Ptr(filepath)}

	switch status.Worktree {
	case git.Deleted:
		options.Action = gitlab.Ptr(gitlab.FileDelete)

	case git.Modified, git.Untracked:
		switch status.Worktree {
		case git.Modified:
			options.Action = gitlab.Ptr(gitlab.FileUpdate)
		case git.Untracked:
			options.Action = gitlab.Ptr(gitlab.FileCreate)
		}

		options.Encoding = gitlab.Ptr("base64")

		content, err := ReadFileAsBase64(filepath)
		if err != nil {
			return nil, errors.WithStack(err)
		}
		options.Content = gitlab.Ptr(content)

		executable, err := IsExecutableFile(filepath)
		if err != nil {
			return nil, errors.WithStack(err)
		}
		options.ExecuteFilemode = gitlab.Ptr(executable)
	}

	return options, nil
}
