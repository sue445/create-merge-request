package cmr

import (
	"encoding/base64"
	"github.com/cockroachdb/errors"
	"os"
)

// ReadTextFile reads a text file
func ReadTextFile(file string) (string, error) {
	data, err := os.ReadFile(file)

	if err != nil {
		return "", errors.WithStack(err)
	}

	return string(data), nil
}

// ReadFileAsBase64 reads a file and returns its contents as BASE64
func ReadFileAsBase64(file string) (string, error) {
	data, err := os.ReadFile(file)

	if err != nil {
		return "", errors.WithStack(err)
	}

	return base64.StdEncoding.EncodeToString(data), nil
}

// IsExecutableFile checks whether a file is executable
func IsExecutableFile(file string) (bool, error) {
	fileInfo, err := os.Stat(file)
	if err != nil {
		return false, errors.WithStack(err)
	}

	// Check if the file mode has the executable bit set for the user, group, or others
	mode := fileInfo.Mode()
	if mode&0111 != 0 {
		return true, nil
	}
	return false, nil
}
