package main_test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/sue445/create-merge-request/cmd/create-merge-request"
	"testing"
)

func TestGenerateDefaultAuthorEmail(t *testing.T) {
	type args struct {
		userName   string
		serverHost string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "userName and serverHost are set",
			args: args{
				userName:   "user",
				serverHost: "dummy.com",
			},
			want: "user@noreply.dummy.com",
		},
		{
			name: "userName and serverHost aren't set",
			args: args{
				userName:   "",
				serverHost: "",
			},
			want: "create-merge-request@noreply.example.com",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := main.GenerateDefaultAuthorEmail(tt.args.userName, tt.args.serverHost)
			assert.Equal(t, tt.want, got)
		})
	}
}
