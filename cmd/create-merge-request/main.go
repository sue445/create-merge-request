package main

import (
	"fmt"
	"github.com/cockroachdb/errors"
	"github.com/urfave/cli/v2"
	"gitlab.com/sue445/create-merge-request"
	"log"
	"os"
)

var (
	// Version represents app version (injected from ldflags)
	Version string

	// Revision represents app revision (injected from ldflags)
	Revision string
)

func printVersion() {
	fmt.Println(getVersion())
}

func getVersion() string {
	return fmt.Sprintf("create-merge-request %s (revision %s)", Version, Revision)
}

// GenerateDefaultAuthorEmail generate default author email (e.g. user_name@noreply.gitlab.com)
func GenerateDefaultAuthorEmail(userName string, serverHost string) string {
	if userName == "" {
		userName = "create-merge-request"
	}

	if serverHost == "" {
		serverHost = "example.com"
	}

	return fmt.Sprintf("%s@noreply.%s", userName, serverHost)
}

func main() {
	cli.VersionPrinter = func(_ *cli.Context) {
		printVersion()
	}

	params := &cmr.ExecuteParams{}
	sourceBranchSuffix := ""
	authorEmail := ""

	app := &cli.App{
		Usage:   "Create a Merge Request for changes to your GitLab project",
		Version: Version,
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:        "gitlab-api-endpoint",
				Usage:       "GitLab API Endpoint (e.g. https://gitlab.com/api/v4)",
				EnvVars:     []string{"GITLAB_API_ENDPOINT", "CI_API_V4_URL"},
				Required:    true,
				Destination: &params.GitLabParams.APIEndpoint,
			},
			&cli.StringFlag{
				Name:        "gitlab-access-token",
				Usage:       "GitLab access token",
				EnvVars:     []string{"GITLAB_ACCESS_TOKEN"},
				Required:    true,
				Destination: &params.GitLabParams.PrivateToken,
			},
			&cli.StringFlag{
				Name:        "gitlab-project",
				Usage:       "GitLab Project Path (e.g. gitlab-org/gitlab)",
				EnvVars:     []string{"GITLAB_PROJECT", "CI_PROJECT_PATH"},
				Required:    true,
				Destination: &params.ProjectName,
			},
			&cli.StringFlag{
				Name:        "author-email",
				Usage:       "author email for commit",
				Required:    false,
				DefaultText: "${GITLAB_USER_LOGIN}@noreply.${CI_SERVER_HOST} (e.g. create-merge-request@noreply.example.com)",
				Value:       "",
				Destination: &authorEmail,
			},
			&cli.StringFlag{
				Name:        "author-name",
				Usage:       "author name for commit",
				EnvVars:     []string{"GITLAB_USER_NAME"},
				Required:    false,
				DefaultText: "create-merge-request",
				Value:       "create-merge-request",
				Destination: &params.AuthorName,
			},
			&cli.StringFlag{
				Name:        "commit-message",
				Aliases:     []string{"m"},
				Usage:       "commit message",
				Required:    false,
				DefaultText: "[create-merge-request] automated change",
				Value:       "[create-merge-request] automated change",
				Destination: &params.CommitMessage,
			},
			&cli.StringFlag{
				Name:        "source-branch",
				Usage:       "Merge Request branch name",
				Required:    false,
				DefaultText: "create-merge-request/patch",
				Value:       "create-merge-request/patch",
				Destination: &params.SourceBranch.Name,
			},
			&cli.StringFlag{
				Name:        "source-branch-suffix",
				Usage:       "Merge Request branch name suffix (none,random,timestamp,short-commit-hash)",
				Required:    false,
				DefaultText: "none",
				Value:       "none",
				Destination: &sourceBranchSuffix,
			},
			&cli.StringFlag{
				Name:        "target-branch",
				Usage:       "Send Merge Request to this branch (e.g. main, master)",
				EnvVars:     []string{"CI_DEFAULT_BRANCH"},
				Required:    false,
				DefaultText: "main",
				Value:       "main",
				Destination: &params.TargetBranch,
			},
			&cli.StringFlag{
				Name:        "title",
				Aliases:     []string{"t"},
				Usage:       "Merge Request title",
				Required:    false,
				DefaultText: "Changes by create-merge-request",
				Value:       "Changes by create-merge-request",
				Destination: &params.Title,
			},
			&cli.StringFlag{
				Name:        "description",
				Usage:       "Merge Request description",
				Required:    false,
				DefaultText: "Automated changes by [create-merge-request](https://gitlab.com/sue445/create-merge-request)",
				Value:       "Automated changes by [create-merge-request](https://gitlab.com/sue445/create-merge-request)",
				Destination: &params.Description,
			},
			&cli.StringSliceFlag{
				Name:        "labels",
				Usage:       "Merge Request labels",
				Required:    false,
				DefaultText: "",
			},
			&cli.StringSliceFlag{
				Name:        "assignees",
				Usage:       "Merge Request assignees (e.g. user1,user2)",
				Required:    false,
				DefaultText: "",
			},
			&cli.StringSliceFlag{
				Name:        "reviewers",
				Usage:       "Merge Request reviewers (e.g. user1,user2)",
				Required:    false,
				DefaultText: "",
			},
			&cli.BoolFlag{
				Name:        "auto-merge",
				Usage:       "Merge the Merge Request when the pipeline succeeds",
				Required:    false,
				DefaultText: "disabled",
				Value:       false,
				Destination: &params.AutoMerge,
			},
			&cli.StringFlag{
				Name:        "log-level",
				Usage:       "Log level (e.g. DEBUG, INFO, WARN, ERROR)",
				Required:    false,
				DefaultText: "INFO",
				Value:       "INFO",
				Destination: &params.GitLabParams.LogLevel,
			},
		},
		Action: func(context *cli.Context) error {
			params.Labels = context.StringSlice("labels")
			params.Assignees = context.StringSlice("assignees")
			params.Reviewers = context.StringSlice("reviewers")

			if authorEmail == "" {
				params.AuthorEmail = GenerateDefaultAuthorEmail(os.Getenv("GITLAB_USER_LOGIN"), os.Getenv("CI_SERVER_HOST"))
			} else {
				params.AuthorEmail = authorEmail
			}

			var err error
			params.SourceBranch.Suffix, err = cmr.ToBranchSuffix(sourceBranchSuffix)
			if err != nil {
				return errors.WithStack(err)
			}

			return cmr.Execute(params)
		},
		Commands: []*cli.Command{},
	}

	app.CustomAppHelpTemplate = `NAME:
   {{.Name}} - {{.Usage}}

USAGE:
   {{.HelpName}} [command options]

REQUIRED PARAMETERS:
{{range .VisibleFlags}}{{if .Required}}   {{.}}
{{end}}{{end}}
OPTIONAL PARAMETERS:
{{range .VisibleFlags}}{{if not .Required}}   {{.}}
{{end}}{{end}}
`

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
