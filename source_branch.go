package cmr

import (
	"fmt"
	"github.com/cockroachdb/errors"
	"math/rand"
	"time"
)

const (
	randomStringChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
)

// SourceBranch represents source branch options
type SourceBranch struct {
	Name   string
	Suffix BranchSuffix
}

// GenerateBranchName returns source branch name from Name and Suffix
func (s *SourceBranch) GenerateBranchName(gitRepo string) (string, error) {
	switch s.Suffix {
	case None:
		return s.Name, nil

	case Random:
		return fmt.Sprintf("%s-%s", s.Name, GenerateRandomString()), nil

	case Timestamp:
		unixTime := time.Now().Unix()
		return fmt.Sprintf("%s-%d", s.Name, unixTime), nil

	case ShortCommitHash:
		hash, err := GetCurrentShortHash(gitRepo)
		if err != nil {
			return "", errors.WithStack(err)
		}
		return fmt.Sprintf("%s-%s", s.Name, hash), nil
	}
	return "", nil
}

// GenerateRandomString generate random string
func GenerateRandomString() string {
	length := 7

	randomString := make([]byte, length)
	for i := range randomString {
		randomString[i] = randomStringChars[rand.Intn(len(randomStringChars))]
	}

	return string(randomString)
}
