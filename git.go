package cmr

import (
	"github.com/cockroachdb/errors"
	"github.com/go-git/go-git/v5"
)

// GetUntrackedFiles returns untracked files
func GetUntrackedFiles(gitRepo string) (git.Status, error) {
	repo, err := git.PlainOpen(gitRepo)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	worktree, err := repo.Worktree()
	if err != nil {
		return nil, errors.WithStack(err)
	}

	status, err := worktree.Status()
	if err != nil {
		return nil, errors.WithStack(err)
	}

	return status, nil
}

// GetCurrentShortHash returns short hash of current revision
func GetCurrentShortHash(gitRepo string) (string, error) {
	repo, err := git.PlainOpen(gitRepo)
	if err != nil {
		return "", errors.WithStack(err)
	}

	head, err := repo.Head()
	if err != nil {
		return "", errors.WithStack(err)
	}

	commit, err := repo.CommitObject(head.Hash())
	if err != nil {
		return "", errors.WithStack(err)
	}

	return commit.Hash.String()[:7], nil
}
