<details>
  <summary>TOC</summary>

[[_TOC_]]

</details>

## Unreleased
[full changelog](https://gitlab.com/sue445/create-merge-request/compare/v0.3.5...main)

## [v0.3.5](https://gitlab.com/sue445/create-merge-request/-/releases/v0.3.5)
[full changelog](https://gitlab.com/sue445/create-merge-request/compare/v0.3.4...v0.3.5)

* Upgrade to Go 1.24
  * https://gitlab.com/sue445/create-merge-request/-/merge_requests/103
* Update dependencies

## [v0.3.4](https://gitlab.com/sue445/create-merge-request/-/releases/v0.3.4)
[full changelog](https://gitlab.com/sue445/create-merge-request/compare/v0.3.3...v0.3.4)

* Migrate to gitlab.com/gitlab-org/api/client-go
  * https://gitlab.com/sue445/create-merge-request/-/merge_requests/88
* Update dependencies

## [v0.3.3](https://gitlab.com/sue445/create-merge-request/-/releases/v0.3.3)
[full changelog](https://gitlab.com/sue445/create-merge-request/compare/v0.3.2...v0.3.3)

* Build binaries for releases with Go 1.23
  * https://gitlab.com/sue445/create-merge-request/-/merge_requests/87
* Update dependencies

## [v0.3.2](https://gitlab.com/sue445/create-merge-request/-/releases/v0.3.2)
[full changelog](https://gitlab.com/sue445/create-merge-request/compare/v0.3.1...v0.3.2)

* Downgrade go version in `go.mod`
  * https://gitlab.com/sue445/create-merge-request/-/merge_requests/68

## [v0.3.1](https://gitlab.com/sue445/create-merge-request/-/releases/v0.3.1)
[full changelog](https://gitlab.com/sue445/create-merge-request/compare/v0.3.0...v0.3.1)

* Upgrade to Go 1.23
  * https://gitlab.com/sue445/create-merge-request/-/merge_requests/67
* Update dependencies

## [v0.3.0](https://gitlab.com/sue445/create-merge-request/-/releases/v0.3.0)
[full changelog](https://gitlab.com/sue445/create-merge-request/compare/v0.2.5...v0.3.0)

* Add `--log-level` and print logs during GitLab API execution
  * https://gitlab.com/sue445/create-merge-request/-/merge_requests/64
* Update dependencies

## [v0.2.5](https://gitlab.com/sue445/create-merge-request/-/releases/v0.2.5)
[full changelog](https://gitlab.com/sue445/create-merge-request/compare/v0.2.4...v0.2.5)

* Wrap all errors with `errors.WithStack`
  * https://gitlab.com/sue445/create-merge-request/-/merge_requests/59

## [v0.2.4](https://gitlab.com/sue445/create-merge-request/-/releases/v0.2.4)
[full changelog](https://gitlab.com/sue445/create-merge-request/compare/v0.2.3...v0.2.4)

* nits: Wrap error with `errors.WithStack`
  * https://gitlab.com/sue445/create-merge-request/-/merge_requests/54

## [v0.2.3](https://gitlab.com/sue445/create-merge-request/-/releases/v0.2.3)
[full changelog](https://gitlab.com/sue445/create-merge-request/compare/v0.2.2...v0.2.3)

* Fixed permissions are lost when committing executable files
  * https://gitlab.com/sue445/create-merge-request/-/merge_requests/50

## [v0.2.2](https://gitlab.com/sue445/create-merge-request/-/releases/v0.2.2)
[full changelog](https://gitlab.com/sue445/create-merge-request/compare/v0.2.1...v0.2.2)

* Fix Bad Request error when trying to commit binary files
  * https://gitlab.com/sue445/create-merge-request/-/merge_requests/46

## [v0.2.1](https://gitlab.com/sue445/create-merge-request/-/releases/v0.2.1)
[full changelog](https://gitlab.com/sue445/create-merge-request/compare/v0.2.0...v0.2.1)

* Use environment variables on GitLab CI as default values for `--author-email` and `--author-name`
  * https://gitlab.com/sue445/create-merge-request/-/merge_requests/37

## [v0.2.0](https://gitlab.com/sue445/create-merge-request/-/releases/v0.2.0)
[full changelog](https://gitlab.com/sue445/create-merge-request/compare/v0.1.1...v0.2.0)

* Add `--auto-merge`
  * https://gitlab.com/sue445/create-merge-request/-/merge_requests/34

## [v0.1.1](https://gitlab.com/sue445/create-merge-request/-/releases/v0.1.1)
[full changelog](https://gitlab.com/sue445/create-merge-request/compare/v0.1.0...v0.1.1)

* nits: MergeRequest -> Merge Request
  * https://gitlab.com/sue445/create-merge-request/-/merge_requests/27

## [v0.1.0](https://gitlab.com/sue445/create-merge-request/-/releases/v0.1.0)
* first release
